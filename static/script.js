$(document).ready(function($){
  $('.accordion_box:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
  $('.accordion_box:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
  $('.accordion_box:first').children('.acc_trigger').addClass('selected').next('.acc_container').show()

  $('.acc_trigger').click(function(event){
      if($(this).hasClass('selected')){
          $(this).removeClass('selected');
          $(this).children('i').removeClass('fa-chevron-down');
          $(this).next().slideUp();
          $(this).parent().removeClass('activate');
      }else{
          $('.acc_trigger').removeClass('selected');
          $(this).addClass('selected');
          $('.acc_trigger').children('i').removeClass('fa-chevron-down');
          $(this).children('i').addClass('fa-chevron-down');
          $('.acc_trigger').next().slideUp();
          $(this).next().slideDown();
          $('.accordion_box').removeClass('active');
          $(this).parent().removeClass('active');

      }
      
  });
});

$(".reorder-up").click(function(){
  var $current = $(this).closest('.wrapper')
  var $previous = $current.prev('.wrapper');
  if($previous.length !== 0){
    $current.insertBefore($previous);
  }
  return false;
});

$(".reorder-down").click(function(){
  var $current = $(this).closest('.wrapper')
  var $next = $current.next('.wrapper');
  if($next.length !== 0){
    $current.insertAfter($next);
  }
  return false;
});