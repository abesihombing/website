$(document).ready(function($){
  $('.accordion_box:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
  $('.accordion_box:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
  $('.accordion_box:first').children('.acc_trigger').addClass('selected').next('.acc_container').show()

  $('.acc_trigger').click(function(event){
      if($(this).hasClass('selected')){
          $(this).removeClass('selected');
          $(this).children('i').removeClass('fa-chevron-down');
          $(this).next().slideUp();
          $(this).parent().removeClass('activate');
      }else{
          $('.acc_trigger').removeClass('selected');
          $(this).addClass('selected');
          $('.acc_trigger').children('i').removeClass('fa-chevron-down');
          $(this).children('i').addClass('fa-chevron-down');
          $('.acc_trigger').next().slideUp();
          $(this).next().slideDown();
          $('.accordion_box').removeClass('active');
          $(this).parent().removeClass('active');

      }
      
  });
});

$(".reorder-up").click(function(){
  var $current = $(this).closest('.wrapper')
  var $previous = $current.prev('.wrapper');
  if($previous.length !== 0){
    $current.insertBefore($previous);
  }
  return false;
});

$(".reorder-down").click(function(){
  var $current = $(this).closest('.wrapper')
  var $next = $current.next('.wrapper');
  if($next.length !== 0){
    $current.insertAfter($next);
  }
  return false;
});

$(".keyword").keyup(function(){
  var kata_kunci = $(".keyword").val();
  var url_utk_dipanggil = "json_call/?q="+ kata_kunci;   
  $.ajax({
    url:url_utk_dipanggil,
    success:function(hasil){ 
      var obj_hasil = $("#hasil");
          obj_hasil.empty();
         for(i=0;i< hasil.items.length;i++){
          var id = hasil.items[i].id;
          var judul_buku = hasil.items[i].volumeInfo.title;
          var author = !hasil.items[i].volumeInfo.authors ? "Unknown": hasil.items[i].volumeInfo.authors.join(", ");
          var publisher= !hasil.items[i].volumeInfo.publisher ? "Unknown" : hasil.items[i].volumeInfo.publisher;
          var publishedDate=!hasil.items[i].volumeInfo.publishedDate ? "Unknown" : hasil.items[i].volumeInfo.publishedDate;
          var image=hasil.items[i].volumeInfo.imageLinks;
          var description=hasil.items[i].volumeInfo.description;
          var link= hasil.items[i].volumeInfo.infoLink;
          console.log(judul_buku);
          console.log(author);
          const HTMLText = 
        `    
        <tr><tr > <th scope="row" class=" align-middle">${i + 1}</th>
                        <td><img class='img-fluid' style='width:22vh' src="${hasil.items[i].volumeInfo.imageLinks.smallThumbnail}"></img></td>
                        <td class='align-middle'>${hasil.items[i].volumeInfo.title} </td>
                        <td class='align-middle'> ${hasil.items[i].volumeInfo.authors}</td>
                        <td class='align-middle'> ${hasil.items[i].volumeInfo.publisher}</td> 

      

        `
          obj_hasil.append(HTMLText)
      }
    },
  });
});

// "https://www.googleapis.com/books/v1/volumes?q=" //