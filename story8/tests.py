from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import buka_story_8,json_call

class Test_Story8(TestCase):
    def test_url_exist(self):
        response = Client().get('/story8')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/story8')
        self.assertTemplateUsed(response,'story8.html')

    def test_function_used(self):
        response = Client().get('/story8')
        found = resolve('/story8')
        self.assertEqual(found.func, buka_story_8)

    def test_json_url(self):
        response = Client().get('/json_call/')
        self.assertEqual(response.status_code,200)


