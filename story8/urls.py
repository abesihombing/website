from django.contrib import admin
from django.urls import path ,include
from .views import buka_story_8,json_call

urlpatterns = [
    path('',buka_story_8),
    path('json_call/',json_call),
]